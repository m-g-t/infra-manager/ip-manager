<?php
include_once '../.htsettings.php';
$login_required = !$permission_anon_read;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['range']))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('SELECT ip, status FROM ip WHERE ip_range = ?');
if(!$stmt)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit;
}
$stmt->bind_param("s", $_POST['range']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $stmt->close();
    $conn->close();
    exit;
}
if(!$row = $result->fetch_assoc())
{
    echo '{"result" : "error", "error" : "No data"}';
}
else
{
    echo "{
    \"result\" : \"OK\",
    \"list\" : [
        {\"ip\" : \"$_POST[range].$row[ip]\", \"status\" : \"$row[status]\"}";
    while($row = $result->fetch_assoc())
    {
        echo ",
        {\"ip\" : \"$_POST[range].$row[ip]\", \"status\" : \"$row[status]\"}";
    }
    echo '
    ]
}';
}
?>