<?php
$login_required = true;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['range']))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('DELETE FROM ip WHERE ip_range = ?');
if(!$stmt)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_POST['range']);
$stmt->execute();
$stmt->close();
$conn->close();
echo '{"result" : "OK"}';
?>