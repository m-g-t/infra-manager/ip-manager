<?php
$login_required = false;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST' || isset($_POST['id'], $_POST['pw']))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('SELECT HEX(salt) s FROM users WHERE name = ?');
if(!$stmt)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_POST['id']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $salt = hex2bin($result->fetch_assoc()['s']);
}
else
{
    $result->close();
    $stmt->close();
    $conn->close();
    echo '{"result" : "No members match ID"}';
    exit;
}
$result->close();
$stmt->close();
$stmt = $conn->prepare('SELECT seq FROM users WHERE name = ? AND pw = UNHEX(?)');
if(!$stmt)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit;
}
$pass = bin2hex(hash_pbkdf2('sha256', $_POST['pw'], $salt, 1000, 64, true));
$stmt->bind_param('ss', $_POST['id'], $pass);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $seq = $result->fetch_assoc()['seq'];
}
else
{
    $result->close();
    $stmt->close();
    $conn->close();
    echo '{"result" : "PW does not match"}';
    exit;
}
$result->close();
$stmt->close();
$conn->close();
session_start();
$_SESSION['seq'] = $seq;
echo '{"result" : "OK"}';
?>