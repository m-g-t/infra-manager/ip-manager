<?php
include_once '../.htsettings.php';
header('Content-Type: application/json; charset=utf-8');
if($https_only && stripos($_SERVER['SERVER_PROTOCOL'], 'https') === false)
{
    echo '{"result" : "error", "error" : "HTTPS only"}';
    exit;
}
if($login_required)
{
    session_start();
    if(!isset($_SESSION['seq']))
    {
        echo '{"result" : "error", "error" : "Login required"}';
        exit;
    }
}
?>