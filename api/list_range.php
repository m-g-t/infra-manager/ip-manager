<?php
include_once '../.htsettings.php';
$login_required = !$permission_anon_read;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['range']))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
include '.htdbconfig.php';
$result = $conn->query('SELECT ip_range FROM ip GROUP BY ip_range');
if(!$result)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit();
}
if(!$row = $result->fetch_assoc())
{
    echo '{"result" : "error", "error" : "No data"}';
}
else
{
    echo "{
    \"result\" : \"OK\",
    \"list\" : [
        {\"ip\" : \"$row[ip_range].xxx\"}";
    while($row = $result->fetch_assoc())
    {
        echo ",
        {\"ip\" : \"$row[ip_range].xxx\"}";
    }
    echo '
    ]
}';
}
?>