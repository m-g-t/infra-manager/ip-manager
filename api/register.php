<?php
$login_required = false;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
include '.htdbconfig.php';
if(isset($_POST['id'], $_POST['pw']))
{
    $stmt = $conn->prepare('INSERT IGNORE INTO users (name, salt, pw) VALUES (?, UNHEX(?), UNHEX(?))');
    if(!$stmt)
    {
        echo '{"result" : "error", "error" : "DB error"}';
        $conn->close();
        exit;
    }
    $salt = random_bytes(64);
    $pass = bin2hex(hash_pbkdf2('sha256', $_POST['pw'], $salt, 1000, 64, true));
    $salt = bin2hex($salt);
    $stmt->bind_param('sss', $_POST['id'], $salt, $pass);
    $stmt->execute();
    if($stmt->affected_rows === 1)
    {
        echo '{"result" : "OK"}';
    }
    else
    {
        echo '{"result" : "error", "error" : "Already exists"}';
    }
    $stmt->close();
}
else
{
    echo '{"result" : "error", "error" : "Wrong access"}';
}
$conn->close();
?>