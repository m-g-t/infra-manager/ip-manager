<?php
$login_required = true;
include '.htheader.php';
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['range']))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
$ip = explode(".", $_POST['range']);
if(count($ip) <= 3 || !is_numeric($ip[0]) || !is_numeric($ip[1]) || !is_numeric($ip[2]))
{
    echo '{"result" : "error", "error" : "Wrong access"}';
    exit;
}
$ip0 = intval($ip[0]);
$ip1 = intval($ip[1]);
$ip2 = intval($ip[2]);
if($ip0 == 192 && $ip1 == 168)
{
    $ip = "192.168.$ip2";
}
else if($ip0 == 172 && $ip1 >= 16 && $ip1 <= 31)
{
    $ip = "172.$ip1.$ip2";
}
else if($ip0 == 10)
{
    $ip = "10.$ip1.$ip2";
}
else
{
    echo '{"result" : "error", "error" : "Invalid IP"}';
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare("INSERT INTO ip (ip_range, ip) VALUES (\"$ip\", ?)");
if(!$stmt)
{
    echo '{"result" : "error", "error" : "DB error"}';
    $conn->close();
    exit;
}
for($i = 1; $i < 255; $i++)
{
    $stmt->bind_param('i', $i);
    $stmt->execute();
}
$stmt->close();
$conn->close();
echo '{"result" : "OK"}';
?>