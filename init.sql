CREATE TABLE users
(
    seq INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL UNIQUE,
    salt BINARY(64) NOT NULL,
    pw BINARY(64) NOT NULL
);

CREATE TABLE ip
(
    ip_range CHAR(11) NOT NULL,
    ip INT NOT NULL,
    status INT NOT NULL DEFAULT 0,
    owner INT NULL,
    description VARCHAR(100) NULL,
    KEY(status, owner),
    KEY(owner, status),
    KEY(description),
    PRIMARY KEY(ip_range, ip)
);
