<?php
include_once '.htsettings.php';
if($https_only && stripos($_SERVER['SERVER_PROTOCOL'],'https') === false)
{
    header("Location: https://$_SERVER[HTTP_HOST]");
    exit;
}
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>IP Manager</title>
